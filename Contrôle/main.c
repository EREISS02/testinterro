#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    srand(time(NULL));

    printf("Vous vous reveillez dans un casino tenu par la mafia russe.\nApres une serie de malchance, vous etes endette de 100 jetons.\nVous echangez votre montre contre 10 jetons.\nSi vous souhaitez vous en sortir en vie, il va falloir jouer a la roulette... ou la roulette russe.\n");

    unsigned int jetons = 10; //nombre de jetons avec lesquels le joueur commence
    const int jetonsnecessaires = 100; //nombre de jetons que le joueur devra atteindre pour pouvoir sortir du casino
    unsigned int roulette; //nombre qui sera g�n�r� al�atoirement
    unsigned int miseroulette; //case sur laquelle le joueur mise
    unsigned int misevalide =1; //test pour savoir si la mise est comprise en 1 et 37
    unsigned int misejetons; //nombre de jetons que le joueur souhaitera miser
    unsigned int rouletterussejouer = 1; //si le joueur veut continuer la roulette russe
    unsigned int rouletterusse; //nombre al�atoire compris entre 1 et 6

    while (jetons < jetonsnecessaires) //tant que le joueur n'a pas 100 jetons
    {
        while (jetons > 0 && jetons < jetonsnecessaires) //tant que le joueur a des jetons, et moins de 100
        {

            misevalide = 1;
            while(misevalide == 1 && jetons < jetonsnecessaires)
            {
                printf("Sur quelle case souhaitez-vous miser ?\n");
                scanf("%d", &miseroulette); //demande la case o� le joueur souhaite miser ses jetons
                printf("Vous possedez %d jetons\n", jetons);
                printf("Combien de jetons voulez-vous miser ?\n");
                scanf("%d", &misejetons); //demande combien de jetons le joueur souhaite miser

                if (miseroulette<1 || miseroulette>37 || misejetons>jetons || misejetons>25 || misejetons<1) //test si la mise est valide ou non
                {
                    printf("Votre mise n'est pas valide.\n");
                    misevalide=0;
                }
                while (misevalide==1 && jetons < jetonsnecessaires) //si la mise est valide
                {
                    jetons = jetons - misejetons; //le joueur perd sa mise
                    roulette = rand()%38; //g�n�ration du nombre al�atoire de la roulette
                    printf("Numero de la roulette : %d\n", roulette);
                    if (roulette ==0)
                    {
                        printf("Vous perdez votre mise.\n");
                        misevalide = 0; //sort des deux boucles pour relancer une partie (tant qu'il reste des jetons au joueur)
                    }
                    else
                    {
                        if (roulette%2 == 0 && miseroulette%2 == 0)
                        {
                            printf("Vous gagnez le double de votre mise.\n");
                            jetons = jetons + misejetons * 2;
                            misevalide = 0; //sort des deux boucles pour relancer une partie (tant qu'il reste des jetons au joueur)
                        }
                        else
                        {
                            if (roulette%2 != 0 && miseroulette%2 != 0)
                            {
                                printf("Vous gagnez le double de votre mise.\n");
                                jetons = jetons + misejetons * 2; //le joueur gagne le double de sa mise
                                misevalide = 0; //sort des deux boucles pour relancer une partie (tant qu'il reste des jetons au joueur)
                            }
                            else
                            {
                                printf("Vous perdez votre mise.\n");
                                misevalide = 0; //sort des deux boucles pour relancer une partie (tant qu'il reste des jetons au joueur)
                            }

                        }
                    }
                }
            }
        }
        rouletterussejouer = 1;
        while(jetons ==0)
        {
            printf("Etant arrive a 0 jeton, la mafia vous force a jouer a la roulette russe pour sauver votre peau.\nVous gagnerez 20 jetons si vous survivez.\nUne goutte de sueur coule sur votre front...\n");
            rouletterusse = rand()%6+1;
            while (rouletterussejouer == 1)
            {
                if (rouletterusse == 1) //test si le coup part
                {
                    printf("Bienvenue au paradis.\n");
                    return 0;
                }
                else //si le coup ne part pas
                {
                    printf("Vous entendez un *clic* et survivez...\n");
                    rouletterusse --; //le barillet tourne, la balle se rapproche
                    jetons = jetons + 20; //le joueur gagne 20 jetons
                    printf("Voulez-vous retenter votre chancer a la roulette russe ?\n(Tapez 1 pour oui, ou un autre nombre pour retourner � la roulette)\n");
                    scanf("%d", &rouletterussejouer); //on demande au joueur s'il souhaite rejour, s'il ne souhaite pas rejouer, il retournera jouer � la roulette

                }
            }
        }
    }
    printf("La mafia vous casse le bras avant de vous relacher, vous suspectant d'avoir triche.\n");
    return 0;
}
